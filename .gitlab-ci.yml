stages:
  - prepare
  - build
  - deploy

image: alpine:3.6

variables:
  OFFICIAL_URI: ${CI_PROJECT_URL}

prepare:branches:
  stage: prepare
  variables:
    GIT_STRATEGY: none
  script:
    - echo 'export REVNUMBER="${CI_COMMIT_REF_NAME}-${CI_JOB_ID}"' >> environments
    - echo 'export REVDATE="`LANG=C date +\"%B %d, %Y\"`"' >> environments
    - echo 'export REVREMARK="${CI_COMMIT_REF_NAME}"' >> environments
  except:
    - tags
  artifacts:
    paths:
      - environments

prepare:release:
  stage: prepare
  variables:
    GIT_STRATEGY: none
  script:
    - echo 'export REVNUMBER="${CI_COMMIT_REF_NAME}"' >> environments
    - echo 'export REVDATE="`LANG=C date +\"%B %d, %Y\"`"' >> environments
    - echo 'export REVREMARK="初版"' >> environments
  only:
    - tags
  artifacts:
    paths:
      - environments

build:html:
  stage: build
  image: asciidoctor/docker-asciidoctor
  before_script:
    - . ./environments
  script:
    - |-
      asciidoctor \
        -r asciidoctor-diagram \
        -a imagesoutdir=./ \
        -a imagesdir=./ \
        -a data-uri= \
        -a "revnumber=${REVNUMBER}" \
        -a "revdate=${REVDATE}" \
        -a "revremark=${REVREMARK}" \
        -a official-uri=${OFFICIAL_URI} \
        -o dist/index.html sample/index.adoc
  artifacts:
    paths:
      - dist

build:pdf:
  stage: build
  image: asciidoctor/docker-asciidoctor
  before_script:
    - . ./environments
    - gem install --no-document asciidoctor-pdf-cjk
  script:
    - |-
      asciidoctor-pdf \
        -r asciidoctor-diagram \
        -r asciidoctor-pdf-cjk \
        -a imagesoutdir=./ \
        -a imagesdir=./ \
        -a "revnumber=${REVNUMBER}" \
        -a "revdate=${REVDATE}" \
        -a "revremark=${REVREMARK}" \
        -a official-uri=${OFFICIAL_URI} \
        -b pdf \
        -o dist/asciidoctor-sample.pdf sample/index.adoc
  artifacts:
    paths:
      - dist

build:slide:
  stage: build
  image: asciidoctor/docker-asciidoctor
  before_script:
    - . ./environments
  after_script:
    - rm -rf dist/.asciidoctor
  script:
    - mkdir -p dist
    - cp slide/custom.css dist
    - |-
      asciidoctor-revealjs \
        -r asciidoctor-diagram \
        -a imagesoutdir=./ \
        -a imagesdir=./ \
        -a data-uri= \
        -a "revnumber=${REVNUMBER}" \
        -a "revdate=${REVDATE}" \
        -a "revremark=${REVREMARK}" \
        -a official-uri=${OFFICIAL_URI} \
        -o dist/slide.html slide/slide.adoc
  artifacts:
    paths:
      - dist

.package: &package
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - mv dist/* .
    - rmdir dist

package:master:
  <<: *package
  stage: deploy
  only:
    - master
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-${CI_JOB_ID}
    paths:
      - index.html
      - asciidoctor-sample.pdf
      - slide.html
      - custom.css
    expire_in: '2 weeks'
  environment:
    name: master
    url: ${CI_PROJECT_URL}/builds/artifacts/master/file/index.html?job=package:master

package:branches:
  <<: *package
  stage: deploy
  except:
    - tags
    - master
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-${CI_JOB_ID}
    paths:
      - index.html
      - asciidoctor-sample.pdf
      - slide.html
      - custom.css
    expire_in: '2 weeks'
  environment:
    name: reviews/${CI_COMMIT_REF_NAME}
    url: ${CI_PROJECT_URL}/builds/artifacts/${CI_COMMIT_REF_NAME}/file/index.html?job=package:branches

package:release:
  <<: *package
  stage: deploy
  only:
    - tags
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}
    paths:
      - index.html
      - asciidoctor-sample.pdf
      - slide.html
      - custom.css
    expire_in: '47 yrs'
  environment:
    name: release/${CI_COMMIT_REF_NAME}
    url: ${CI_PROJECT_URL}/builds/artifacts/${CI_COMMIT_REF_NAME}/file/index.html?job=package:branches

pages:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - mv dist public
  artifacts:
    paths:
      - public
  only:
    - tags
